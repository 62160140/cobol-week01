       IDENTIFICATION DIVISION. 
       PROGRAM-ID. PlusTwoNumber.
       DATA DIVISION. 
        WORKING-STORAGE SECTION.
      /    8ตัวอักษร
        01 NUM1 PIC 9(8).  
        01 NUM2 PIC 9(8).
        01 RESULT PIC 9(8).
        PROCEDURE DIVISION.
        MAIN-PROCEDURE.

      *    input1
           DISPLAY "INPUT NUMBER 1:" WITH NO ADVANCING .
           ACCEPT NUM1.

      *    input2
           DISPLAY "INPUT NUMBER 1:" WITH NO ADVANCING .
           ACCEPT NUM2.

      *    COMPUTE
           COMPUTE RESULT =  NUM1 + NUM2.

      *    Display result
           DISPLAY "RESULT = " RESULT .
           STOP RUN.
       END PROGRAM PlusTwoNumber.
